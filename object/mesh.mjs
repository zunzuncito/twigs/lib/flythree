
import * as THREE from 'three';


export default class Mesh {

  constructor(object_3d, cfg) {
    this.animations = [];
    this.three_mesh = object_3d.scene;

    this.mixer = new THREE.AnimationMixer(object_3d.scene);
    for (let i = 0; i < object_3d.animations.length; i++) {
      let clip_cfg = [
        object_3d.animations[i]
      ];
      for (let clip of cfg.animations) {
        if (clip.index == i) {
          if (clip.optional_root) {
            clip_cfg.push(clip.optional_root);
          } else {
            clip_cfg.push(null);
          }
          if (clip.blend_mode) clip_cfg.push(clip.blend_mode);
          break;
        }
      }
      this.animations.push(this.mixer.clipAction(...clip_cfg));
    }
  }

  animate(i) {
    this.animations[i].time = 0;
    this.animations[i].paused = false;
    this.animations[i].enabled = true;
    this.animations[i].play();
  }


  static async construct(object_3d, cfg) {
    try {
      object_3d = await object_3d;
      if (typeof cfg !== "object") cfg = {};
      if (!Array.isArray(cfg.animations)) cfg.animations = [];

      const _this = new Mesh(object_3d, cfg);
      if (typeof cfg.on_load == "function") cfg.on_load(_this);
      return _this;
    } catch (e) {
      console.error(e);
    }
  }
}
