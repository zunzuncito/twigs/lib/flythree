
export default class HeightMap {
  static vertices(width, height, tile_size, height_mult, data) {
    const vertex_count = (width - 1) * (height - 1) * 6;
    const vertices = new Float32Array(vertex_count * 3);

    console.log(width, height, data, tile_size);

    const width_t = width-1;
    const height_t = height-1;

    let highest = 0;

    let cur_tile = 0;
    for (let x = 0; x < width_t; x++) {
      for (let y = 0; y < height_t; y++) {
        let t = y * width + x;
        let v = cur_tile*18;

        if (data[t] > highest) highest = data[t];

        vertices[ v ] = (x - width_t / 2) * tile_size + tile_size;
        vertices[ v + 1 ] = data[t + width + 1] * height_mult;
        vertices[ v + 2 ] = (y - height_t / 2) * tile_size + tile_size;

        vertices[ v + 3 ] = (x - width_t / 2) * tile_size + tile_size;
        vertices[ v + 4 ] = data[t + 1] * height_mult;
        vertices[ v + 5 ] = (y - height_t / 2) * tile_size;

        vertices[ v + 6 ] = (x - width_t / 2) * tile_size;
        vertices[ v + 7 ] = data[t] * height_mult;
        vertices[ v + 8 ] = (y - height_t / 2) * tile_size;

        vertices[ v + 9] = (x - width_t / 2) * tile_size;
        vertices[ v + 10 ] = data[t] * height_mult;
        vertices[ v + 11 ] = (y - height_t / 2) * tile_size;

        vertices[ v + 12 ] = (x - width_t / 2) * tile_size;
        vertices[ v + 13 ] = data[t + width] * height_mult;
        vertices[ v + 14 ] = (y - height_t / 2) * tile_size + tile_size;

        vertices[ v + 15 ] = (x - width_t / 2) * tile_size + tile_size;
        vertices[ v + 16 ] = data[t + width + 1] * height_mult;
        vertices[ v + 17 ] = (y - height_t / 2) * tile_size + tile_size;

        for (let i = 0; i < 18; i++)  {
          var item = vertices[v+i];
          if (isNaN(item)) {
            console.log("NaN", x, y, i);
          }
        }

        cur_tile++;
      }
    }

    console.log("highest_height", highest);

    return vertices;
  }
}
