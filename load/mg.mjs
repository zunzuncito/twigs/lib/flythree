
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js';

export default class LoadingManager {
  constructor(cfg) {
    this.loader = new GLTFLoader().setPath(cfg.path || '/');

    if (cfg.draco) {
      const draco_loader = new DRACOLoader();
      draco_loader.setDecoderPath('draco/');
      loader.setDRACOLoader(draco_loader);
    }

    this.res_queue = [];
  }


  async queue(sub_path, cfg) {
    try {
      if (typeof cfg !== "object") cfg = {};
      cfg.path = sub_path;
      let loaded = false;
      cfg.callback = (object_3d) => {
        loaded = object_3d;
      }
      
      this.res_queue.push(cfg);

      return await new Promise((fulfil) => {
        const interval_id = setInterval(() => {
          if (loaded) {
            clearInterval(interval_id);
            fulfil(loaded);
          }
        }, 100);
      });
    } catch (e) {
      console.error(e);
    }
  }

  async load() {
    try {
      for (let i = 0; i < this.res_queue.length; i++) {
        const queue_item = this.res_queue[i];
        await new Promise((fulfil) => {
          this.loader.load(queue_item.path, async function(gltf) {
            if (typeof queue_item.traverse == "function") gltf.scene.traverse(queue_item.traverse);
            queue_item.callback(gltf);
            fulfil();
          });
        });
      }

      this.res_queue = [];
    } catch (e) {
      console.error(e);
    }
  }
}
