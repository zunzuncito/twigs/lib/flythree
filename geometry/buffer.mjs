
import * as THREE from 'three';

export default class BufferGeometry {
  constructor(attrs) {
    this.geometry = new THREE.BufferGeometry();
    for (let key in attrs) {
      const attr = attrs[key];
      this.geometry.setAttribute(key, new THREE.BufferAttribute(
        attr.data,
        attr.dimensions
      ));
    }
  }
}
