
import * as THREE from 'three';
import BufferGeometry from "./geometry/buffer.mjs"
import HeightMap from "./map/height.mjs"
import Mesh from "./object/mesh.mjs"
import LoadingManager from "./load/mg.mjs"

export default class FlyThree {
  constructor(cfg) {
    this.cfg = cfg || {};

    this.renderer = new THREE.WebGLRenderer(cfg.renderer);
    this.renderer.setPixelRatio( cfg.pixel_ratio || window.devicePixelRatio );
    this.renderer.setSize( cfg.width || window.innerWidth, cfg.height || window.innerHeight );

    this.scene = new THREE.Scene();
    if (cfg.scene.background !== "transparent") {
      this.scene.background = new THREE.Color(cfg.scene.background || 0xbfd1e5);
    }
  }

  static THREE = THREE
  static BufferGeometry = BufferGeometry
  static HeightMap = HeightMap
  static Mesh = Mesh
  static LoadingManager = LoadingManager
}
